<!--
SPDX-FileCopyrightText: 2021 Melvin Keskin <melvo@olomono.de>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Contributing

[[_TOC_]]

## Introduction

There are mainly the following ways to contribute:

1. [Modifiying the **Ansible setup**](#ansible)

All those tools need to be improved and extended.
In order to achieve that, we welcome everybody to contribute to this project.
Feel free to [contact us](https://providers.xmpp.net/contact/)!

## Ansible

This section should help contributors with improving the Ansible setup.

### Setup

Please follow the steps explained in [Kaidan's basic contribution setup](https://invent.kde.org/network/kaidan/-/wikis/setup) if you are unfamiliar with KDE Identity, GitLab or Git.
It should answer most of your questions.
You mostly have to replace `network/kaidan` by `melvo/xmpp-providers-server` in the text where necessary.

Set up everything needed to work inside of your local repository:
```
cd xmpp-providers-server
./setup.sh
```

### Merge Requests (MR)

Currently, Melvin Keskin (@melvo), Edward Maurer (@echolonone) and Daniel Brötzmann (@wurstsalat) are the maintainers of XMPP Providers.
They are responsible for accepting MRs.
Nevertheless, all experienced developers can open MRs.
All project members can review MRs and give feedback on them.

Please stick to the following steps for opening, reviewing and accepting MRs.

#### For Authors

1. Create a new branch to work on it from the [master branch](https://invent.kde.org/melvo/xmpp-providers-server/-/tree/master).
1. Write short commit messages starting with an upper case letter and the imperative.
1. If your commit touches only one file, start the commit message with the filename (without the extension) as in `README: Add section 'Categories'` or `criteria: Require 20 MB as 'maximumHttpFileUploadFileSize' for category A`.
1. If your commit closes an issue, add the line `Closes #<commit-number>` (Example: `Depends on #100`) to the commit message's end after a separating empty line.
1. Split your commits logically.
1. Do not mix unrelated changes in the same MR.
1. Create an MR with the *master* branch as its target.
1. Add `Draft: ` in front of the MR's title as long as you are working on the MR and remove it as soon as it is ready to be reviewed.
1. If an MR needs another MR to work, add the line `Depends on !<mr-number>` (Example: `Depends on !100`) to the MR description's end after a separating empty line.
1. A maintainer and possibly other project members will give you feedback.
1. Improve your MR according to their feedback, push your commits and close open threads via the *Resolve thread* button.
1. If necessary, modify, reorder or squash your commits, rebase them on the master branch and force-push (`git push -f`) the result to the MR's branch.
1. As soon as all threads on your MR are resolved, a maintainer will merge your commits into the *master* branch.

Please do not merge your commits into the *master* branch on your own.
If maintainers approved your MR but have not yet merged it, that probably means that they are waiting for the approval of additional maintainers.
Feel free to ask if anything is unclear.

#### For Reviewers

1. Provide detailed descriptions of found issues to the author.
1. Try to give the author concrete proposals for improving the code via the *Insert suggestion* button while commenting.
1. If the proposals are too complicated, create and push a commit with your proposal to your own fork and open an MR with the author's MR branch as its target.
1. In case you are a maintainer:
	1. If you think that no additional review is needed, make editorial changes (such as squashing the commits) and merge the result directly.
	1. If you would like to get (more) feedback from other maintainers, approve the MR using the *Approve* button and mention other maintainers to review the MR.
1. In case you are not a maintainer but a project members and you think that the MR is ready to be merged, approve the MR using the *Approve* button.

Reviews should be done by at least one maintainer not involved as the MR's author or co-author.
